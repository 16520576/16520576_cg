#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
	RECT rect;
	rect.Left = l;
	rect.Right = r;
	rect.Top = t;
	rect.Bottom = b;

	return rect;
}

CODE Encode(RECT r, Vector2D P)
{
	CODE c = 0;
	if (P.x < r.Left)
		c = c | LEFT;
	if (P.x > r.Right)
		c = c | RIGHT;
	if (P.y < r.Top)
		c = c | TOP;
	if (P.y > r.Bottom)
		c = c | BOTTOM;
	return c;
}

int CheckCase(int c1, int c2)
{
	if (c1 == 0 && c2 == 0)
		return 1;
	if (c1 != 0 && c2 != 0 && c1&c2 != 0)
		return 2;
	return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	CODE c1, c2;


	while (true) {

		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
		int _checkCase = CheckCase(c1, c2);

		// neu duong thang hoan toan nam ben trong
		if (_checkCase == 1) {
			return 0;
		}

		// neu duong thang nam ben ngoai cua so
		if (_checkCase == 2) {
			return 0;
		}

		// neu duong thang cat cua so
		if (_checkCase == 3) {
			ClippingCohenSutherland(r, P1, P2);
			Q1 = P1;
			Q2 = P2;
		}
	}
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	CODE code_out;
	Vector2D P;
	CODE c1, c2;
	c1 = Encode(r, P1);
	c2 = Encode(r, P2);

	// co it nhat mot diem nam ngoai cua so
	if (c1 != 0)
		code_out = c1;
	else
		code_out = c2;

	// truong hop diem nam tren cua so
	if (code_out & TOP) {
		P.x = P1.x + (P2.x - P1.x) * (r.Top - P1.y) / (P2.y - P1.y);
		P.y = r.Top;
	}

	// truong hop diem nam duoi cua so
	else if (code_out & BOTTOM) {
		P.x = P1.x + (P2.x - P1.x) * (r.Bottom - P1.y) / (P2.y - P1.y);
		P.y = r.Bottom;
	}

	// truong hop diem nam ben phai cua so
	else if (code_out & RIGHT) {
		P.y = P1.y + (P2.y - P1.y) * (r.Right - P1.x) / (P2.x - P1.x);
		P.x = r.Right;
	}

	// truong hop diem nam ben trai cua so
	else if (code_out & LEFT) {
		P.y = P1.y + (P2.y - P1.y) * (r.Left - P1.x) / (P2.x - P1.x);
		P.x = r.Left;
	}

	// doi diem
	if (code_out == c1)
		P1 = P;
	else
		P2 = P;
}


int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
	if (p == 0)
	{
		if (q < 0)
			return 1;
		return 0;
	}

	if (p > 0)
	{
		float t = (float)q / p;
		if (t2<t)
			return 1;
		if (t1<t)
			t1 = t;
	}
	else {
		float t = (float)q / p;
		if (t < t1)
			return 0;
		if (t < t2)
			t2 = t;
	}
	return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	float t1, t2;
	double Dx = P2.x - P1.x;
	double Dy = P2.y - P1.y;

	t1 = 0;
	t2 = 1;

	if (SolveNonLinearEquation(-Dx, P1.x - r.Right, t1, t2)) {
		if (SolveNonLinearEquation(Dx, r.Left - P1.x, t1, t2)) {
			if (SolveNonLinearEquation(-Dy, P1.y - r.Bottom, t1, t2)) {
				if (SolveNonLinearEquation(Dy, r.Top - P1.y, t1, t2)) {
					Q1.x = P1.x + t1 * Dx;
					Q1.y = P1.y + t1 * Dy;
					Q2.x = P1.x + t2 * Dx;
					Q2.y = P1.y + t2 * Dy;
					return true;
				}
			}
		}
	}
	return false;
}