#include "Bezier.h"
#include "Line.h"
#include "Circle.h"
#include <iostream>
using namespace std;
SDL_Event eventx;
void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	for (double t = 0; t <= 1; t += 0.0001) {
		int x = (1 - t)*(1 - t)*p1.x + 2 * (1 - t)*t*p2.x + t * t*p3.x;
		int y = (1 - t)*(1 - t)*p1.y + 2 * (1 - t)*t*p2.y + t * t*p3.y;
		SDL_RenderDrawPoint(ren, x, y);
	}

	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	Midpoint_Line(p1.x, p1.y, p2.x, p2.y, ren);
	Midpoint_Line(p3.x, p3.y, p2.x, p2.y, ren);

	MidpointDrawCircle(p1.x, p1.y, 8, ren);
	MidpointDrawCircle(p2.x, p2.y, 8, ren);
	MidpointDrawCircle(p3.x, p3.y, 8, ren);


}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	for (double t = 0; t <= 1; t += 0.0001) {
		int x = (1 - t)*(1 - t)*(1 - t)*p1.x + 3 * (1 - t)*(1 - t)*t*p2.x + 3 * (1 - t)*t * t*p3.x + t * t*t*p4.x;
		int y = (1 - t)*(1 - t)*(1 - t)*p1.y + 3 * (1 - t)*(1 - t)*t*p2.y + 3 * (1 - t)*t * t*p3.y + t * t*t*p4.y;
		SDL_RenderDrawPoint(ren, x, y);
	}

	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	Midpoint_Line(p1.x, p1.y, p2.x, p2.y, ren);
	Midpoint_Line(p2.x, p2.y, p3.x, p3.y, ren);
	Midpoint_Line(p3.x, p3.y, p4.x, p4.y, ren);

	MidpointDrawCircle(p1.x, p1.y, 8, ren);
	MidpointDrawCircle(p2.x, p2.y, 8, ren);
	MidpointDrawCircle(p3.x, p3.y, 8, ren);
	MidpointDrawCircle(p4.x, p4.y, 8, ren);

}

bool isinsideCircle(int xc, int yc, int R, int x, int y) {
	return (x - xc)*(x - xc) + (y - yc)*(y - yc) <= R*R;
}

void DrawCurveMouse2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3) {
	DrawCurve2(ren, p1, p2, p3);

	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event

	bool running = true;
	bool draw = false;
	int mark = 0;

	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&eventx))
		{

			//If the user has Xed out the window
			if (eventx.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}

			if (eventx.type == SDL_MOUSEBUTTONDOWN) {
				int x = eventx.motion.x;
				int y = eventx.motion.y;

				// if inside circle p1
				if (isinsideCircle(p1.x, p1.y, 8, x, y)) {
					mark = 1;
					draw = true;
				}
				if (isinsideCircle(p2.x, p2.y, 8, x, y)) {
					mark = 2;
					draw = true;
				}
				if (isinsideCircle(p3.x, p3.y, 8, x, y)) {
					mark = 3;
					draw = true;
				}


			}

			if (eventx.type == SDL_MOUSEBUTTONUP) {

				if (draw == true) {
					int x = eventx.motion.x;
					int y = eventx.motion.y;

					if (mark == 1) {
						p1.x = x;
						p1.y = y;
					}

					if (mark == 2) {
						p2.x = x;
						p2.y = y;
					}

					if (mark == 3) {
						p3.x = x;
						p3.y = y;
					}

					// xoa hinh hien tai
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					DrawCurve2(ren, p1, p2, p3);
					SDL_RenderPresent(ren);
					mark = 0;
					draw = false;
				}
			}
		}

	}
}


void DrawCurveMouse3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4) {
	DrawCurve3(ren, p1, p2, p3, p4);
	
	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event

	bool running = true;
	bool draw = false;
	int mark = 0;

	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&eventx))
		{

			//If the user has Xed out the window
			if (eventx.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}

			if (eventx.type == SDL_MOUSEBUTTONDOWN) {
				int x = eventx.motion.x;
				int y = eventx.motion.y;

				// if inside circle p1
				if (isinsideCircle(p1.x, p1.y, 8, x, y)) {
					mark = 1;
					draw = true;
				}
				if (isinsideCircle(p2.x, p2.y, 8, x, y)) {
					mark = 2;
					draw = true;
				}
				if (isinsideCircle(p3.x, p3.y, 8, x, y)) {
					mark = 3;
					draw = true;
				}

				if (isinsideCircle(p4.x, p4.y, 8, x, y)) {
					mark = 4;
					draw = true;
				}

			}

			if (eventx.type == SDL_MOUSEBUTTONUP) {
				
				if (draw == true) {
					int x = eventx.motion.x;
					int y = eventx.motion.y;

					if (mark == 1) {
						p1.x = x;
						p1.y = y;
					}

					if (mark == 2) {
						p2.x = x;
						p2.y = y;
					}

					if (mark == 3) {
						p3.x = x;
						p3.y = y;
					}

					if (mark == 4) {
						p4.x = x;
						p4.y = y;
					}
					// xoa hinh hien tai
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					DrawCurve3(ren, p1, p2, p3, p4);
					SDL_RenderPresent(ren);
					mark = 0;
					draw = false;
				}
			}
		}

	}
}

