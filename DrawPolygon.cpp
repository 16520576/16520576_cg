#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int *x = new int[4];
	int *y = new int[4];
	double phi = 0.5*M_PI;

	for (int i = 0; i < 3; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);

		phi = phi + 2 * M_PI / 3;
	}

	
	for (int i = 0; i < 3; i++)
		Midpoint_Line(x[i % 3], y[i % 3], x[(i + 1) % 3], y[(i + 1) % 3], ren);
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int *x = new int[4];
	int *y = new int[4];
	double phi = M_PI / 4;
	for (int i = 0; i < 4; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);

		phi = phi + 2 * M_PI / 4;
	}

	for (int i = 0; i < 4; i++)
		Midpoint_Line(x[i % 4], y[i % 4], x[(i + 1) % 4], y[(i + 1) % 4], ren);
}


void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int *x = new int[5];
	int *y = new int[5];

	double phi = M_PI / 2;

	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);

		phi = phi + 2 * M_PI / 5;
	}
	
	for (int i = 0; i < 5; i++)
		Midpoint_Line(x[i % 5], y[i % 5], x[(i + 1) % 5], y[(i + 1) % 5], ren);
}

void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int *x = new int[6];
	int *y = new int[6];

	double phi = 0;

	for (int i = 0; i < 6; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);

		phi = phi + 2 * M_PI / 6;
	}

	for (int i = 0; i < 6; i++)
		Midpoint_Line(x[i % 6], y[i % 6], x[(i + 1) % 6], y[(i + 1) % 6], ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int *x = new int[5];
	int *y = new int[5];

	double phi = M_PI / 2;

	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);

		phi = phi + 2 * M_PI / 5;
	}

	for (int i = 0; i < 5; i++)
		Midpoint_Line(x[i % 5], y[i % 5], x[(i + 2) % 5], y[(i + 2) % 5], ren);

}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int *x = new int[10];
	int *y = new int[10];

	double phi = M_PI / 2;

	// ve 5 diem lon
	for (int i = 0; i < 10; i += 2) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);

		phi = phi + 2 * M_PI / 5;
	}

	// ve 5 diem nho
	int r = int(R*sin(M_PI / 10) / sin(7 * M_PI / 10));
	double new_phi = M_PI / 5 + M_PI / 2;
		for (int i = 1; i < 10; i += 2) {
			x[i] = xc + int(r*cos(new_phi) + 0.5);
			y[i] = yc - int(r*sin(new_phi) + 0.5);

			new_phi = new_phi + 2 * M_PI / 5;
		}

	for (int i = 0; i < 10; i++)
		Midpoint_Line(x[i % 10], y[i % 10], x[(i + 1) % 10], y[(i + 1) % 10], ren);
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int *x = new int[16];
	int *y = new int[16];

	double phi = M_PI / 2;

	// ve 8 dinh
	for (int i = 0; i < 16; i += 2) {
		x[i] = xc + int(R * cos(phi) + 0.5);
		y[i] = yc - int(R * sin(phi) + 0.5);

		phi += 2 * M_PI / 8;
	}
	// ve 8 dinh nho
	int r = int((R * sin(M_PI / 8) / sin(3 * M_PI / 4)));
	double new_phi = (M_PI/8) + (M_PI/2);
	for (int i = 1; i < 16; i += 2) {
		x[i] = xc + int(r * cos(new_phi) + 0.5);
		y[i] = yc - int(r * sin(new_phi) + 0.5);

		new_phi += 2 * M_PI / 8;
	}

	for (int i = 0; i < 16; i++)
		Midpoint_Line(x[i % 16], y[i % 16], x[(i + 1) % 16], y[(i + 1) % 16], ren);

}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{

	int *x = new int[10];
	int *y = new int[10];

	double phi = startAngle;

	// ve 5 diem lon
	for (int i = 0; i < 10; i += 2) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);

		phi = phi + 2 * M_PI / 5;
	}

	// ve 5 diem nho
	int r = int(R*sin(M_PI / 10) / sin(7 * M_PI / 10));
	double new_phi = M_PI / 5 + phi;
	for (int i = 1; i < 10; i += 2) {
		x[i] = xc + int(r*cos(new_phi) + 0.5);
		y[i] = yc - int(r*sin(new_phi) + 0.5);

		new_phi = new_phi + 2 * M_PI / 5;
	}

	for (int i = 0; i < 10; i++)
		Midpoint_Line(x[i % 10], y[i % 10], x[(i + 1) % 10], y[(i + 1) % 10], ren);

}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	double phi = M_PI / 2;
	while (r > 1) {
		DrawStarAngle(xc, yc, r, phi, ren);
		phi = -phi;
		r = int(r*sin(M_PI / 10) / sin(7 * M_PI / 10));
	}
}
