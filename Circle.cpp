#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{

    SDL_RenderDrawPoint(ren, xc + x, yc + y);

	
    //7 points
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + y, yc + x);
	SDL_RenderDrawPoint(ren, xc + y, yc - x);
	SDL_RenderDrawPoint(ren, xc - y, yc + x);
	SDL_RenderDrawPoint(ren, xc - y, yc - x);
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{

}


void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0;
	int y = R;
	int p = 1 - R;

	Draw8Points(xc, yc, x, y, ren);
	while (x < y) {
		if (p < 0)
			p += 2 * x + 3;
		else {
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		Draw8Points(xc, yc, x, y, ren);
	}
}
